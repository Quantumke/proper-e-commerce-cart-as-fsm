defmodule BackendMtickets.Repo.Migrations.CreateCarts do
  use Ecto.Migration

  def change do
    create table(:carts) do
      add :owner, :string
      add :event, :string
      add :number_of_tickers, :integer
      add :ticket_type, :string

      timestamps()
    end

    create unique_index(:carts, [:event])
  end
end
