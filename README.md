# CartStateMachine

## How it works:
cart moves from 4 states - empty - filled - payed and abadonned

Sample code
```ruby

defmodule YourModule.StateMachine.CartStateMachine do
use Machinery,
states: ["empty","filled","payed","abadonned"],
transitions: %{
  "empty"=>"filled",
  "filled"=>["payed","abadonned"]
}

def guard_function(cart, "filled") do
  #send api to backend to reseve ticket
  true
end

def guard_function(cart, "payed") do
  #check if payment has beed recieved
  #send api to backend to move from  reseved to sold
   true
end

def before_transition(cart, "filled") do
  #resere ticckets
  #assert tickets are available
  cart
end

def after_transition(cart, "abadonned") do
  #remove item from cart
  #unreserv tickets
  cart
end
end```



To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

