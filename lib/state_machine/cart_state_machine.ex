defmodule BackendMtickets.StateMachine.CartStateMachine do
use Machinery,
states: ["empty","filled","payed","abadonned"],
transitions: %{
  "empty"=>"filled",
  "filled"=>["payed","abadonned"]
}

def guard_function(cart, "filled") do
  #send api to backend to reseve ticket
  true
end

def guard_function(cart, "payed") do
  #check if payment has beed recieved
  #send api to backend to move from  reseved to sold
   true
end

def before_transition(cart, "filled") do
  #resere ticckets
  #assert tickets are available
  cart
end

def after_transition(cart, "abadonned") do
  #remove item from cart
  #unreserv tickets
  cart
end
end

