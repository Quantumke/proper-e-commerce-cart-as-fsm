defmodule BackendMticketsWeb.CartController do
  use BackendMticketsWeb, :controller

  def add_toCart(conn, params) do
    IO.inspect(params)
    changeset = BackendMtickets.Cart.changeset(%BackendMtickets.Cart{},params)
    case BackendMtickets.Repo.insert(changeset) do
      {:ok, cart_add} ->
        IO.inspect(cart_add)
        Machinery.transition_to(cart_add, CartStateMachine, "filled")
        json conn |> put_status(:created), %{status: ["Processed request succesfully"]}
      {:error, changeset} ->
        IO.inspect(changeset.errors)
        json conn |> put_status(:bad_request), %{error: ["error processing request"]}
    end
  end

def getCartDetails(conn,params) do
  owner= params["owner"]
  events = BackendMtickets.Cart |> BackendMtickets.Repo.all(owner: owner)
  IO.inspect(length(events))
  json conn |> put_status(:created), %{cart_lenght: length(events),events: Poison.encode!(events)}

end

def updateCart(conn, params) do
  cart_id= params["cart_id"]
  ticket_type=params["ticket_type"]
  number_of_tickets=["number_of_tickets"]
  cart =  BackendMtickets.Cart |> BackendMtickets.Repo.get_by(id: cart_id)
  changeset = BackendMtickets.Cart.changeset(cart, %{number_of_tickers: number_of_tickets, ticket_type: ticket_type})
  BackendMtickets.Repo.update(changeset)
end

def removeCart(conn, params) do
  cart_id= params["cart_id"]
  #case not empty so state is left
  Machinery.transition_to(cart_add, CartStateMachine, "abadonned")
  cart =  BackendMtickets.Cart |> BackendMtickets.Repo.get_by(id: cart_id)
  changeset = BackendMtickets.Cart.changeset(cart, %{id: cart_id})
  BackendMtickets.Repo.delete(changeset)
end

end


#%MyStruct{id: 10} |> MyApp.Repo.delete
