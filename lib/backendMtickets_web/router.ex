defmodule BackendMticketsWeb.Router do
  use BackendMticketsWeb, :router

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/api", BackendMticketsWeb do
    pipe_through :api
    post "/cart/add", CartController, :add_toCart
    post "/cart/get", CartController, :getCartDetails
    post "/cart/update", CartController, :updateCart
    post "/cart/delete", CartController, :removeCart

    options "/cart/get", CartController, :options
    options "/cart/add", CartController, :options
    options "/cart/update", CartController, :options
    options "/cart/delete", CartController, :options


  end
end
