defmodule BackendMtickets.PubSub.Listener do
  use GenServer
  use Phoenix.Channel

  require Logger

  import Poison, only: [decode!: 1]

  @doc """
  Initialize the GenServer
  """
  @spec start_link([String.t], [any])  :: {:ok, pid}
  def start_link(channel, otp_opts \\ []), do: GenServer.start_link(__MODULE__, channel, otp_opts)

  @doc """
  When the GenServer starts subscribe to the given channel
  """
  @spec init([String.t])  :: {:ok, []}
  def init(channel) do
    Logger.debug("Starting #{ __MODULE__ } with channel subscription: #{channel}")
    pg_config = BackendMtickets.Repo.config()
    {:ok, pid} = Postgrex.Notifications.start_link(pg_config)
    {:ok, ref} = Postgrex.Notifications.listen(pid, channel)
    {:ok, {pid, channel, ref}}
  end

  @doc """
  Listen for changes
  """
  def handle_info({:notification, _pid, _ref, "cart_changes", payload}, _state) do
    payload
    |> decode!()
    |> IO.inspect()
    IO.inspect("--------------------------------------------------")
    data=Poison.decode(payload) |>elem(1)
    roomID=data["new_row_data"]["owner"]
    events = BackendMtickets.Cart |> BackendMtickets.Repo.all(owner: roomID)
    IO.inspect(length(events))
    BackendMticketsWeb.Endpoint.broadcast("room:#{roomID}","new_msg",%{body: Poison.encode!(events)})
    IO.inspect("Updated UI")
    # Machinery.transition_to(events,BackendMtickets.StateMachine.CartStateMachine ,"next_state")

    {:noreply, :event_handled}
  end

  def handle_info(_, _state), do: {:noreply, :event_received}
end
