defmodule BackendMtickets.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      BackendMtickets.Repo,
      # Start the endpoint when the application starts
      BackendMticketsWeb.Endpoint,
      # Starts a worker by calling: BackendMtickets.Worker.start_link(arg)
      # {BackendMtickets.Worker, arg},

      worker(
        BackendMtickets.PubSub.Listener,
        ["cart_changes", [name: BackendMtickets.PubSub.Listener]],
        restart: :permanent
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BackendMtickets.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    BackendMticketsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
