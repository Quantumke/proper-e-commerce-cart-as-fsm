defmodule BackendMtickets.Cart do
  use Ecto.Schema
  import Ecto.Changeset

  schema "carts" do
    field :event, :string
    field :number_of_tickers, :integer
    field :owner, :string
    field :ticket_type, :string

    timestamps()
  end

  @doc false
  def changeset(cart, attrs) do
    cart
    |> cast(attrs, [:owner, :event, :number_of_tickers, :ticket_type])
    |> validate_required([:owner, :event, :number_of_tickers, :ticket_type])
  end
end
