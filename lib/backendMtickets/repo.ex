defmodule BackendMtickets.Repo do
  use Ecto.Repo,
    otp_app: :backendMtickets,
    adapter: Ecto.Adapters.Postgres
end
