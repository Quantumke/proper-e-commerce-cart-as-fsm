# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config
config :backendMtickets,
  ecto_repos: [BackendMtickets.Repo]

# Configures the endpoint
config :backendMtickets, BackendMticketsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mBOSgSiHRKTl/J+o2PBW9D5R4ixhJQs9g9ntvIdrIn1owabL5LM7hFF053jX71dJ",
  render_errors: [view: BackendMticketsWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BackendMtickets.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]
  config :cors_plug,
  origin: "*",
  max_age: 86400,
  methods: ["GET", "POST"]
# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
config :machinery,
  interface: true,
  repo: BackendMtickets.Repo,
  model: BackendMtickets.Cart,
  module: BackendMtickets.StateMachine.CartStateMachine

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
